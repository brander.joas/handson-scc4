package br.com.scc4.handson.model;

import java.util.List;

public class Mensagem {
	private Remetente remetente;
	private List<Destinatario> destinatarios;
	private String mensagem;
	
	public Remetente getRemetente() {
		return remetente;
	}
	public void setRemetente(Remetente remetente) {
		this.remetente = remetente;
	}
	public List<Destinatario> getDestinatarios() {
		return destinatarios;
	}
	public void setDestinatarios(List<Destinatario> destinatarios) {
		this.destinatarios = destinatarios;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
}
