package br.com.scc4.handson.model;

public class Fracao {
	Integer numerador;
	Integer denominador;
	
	public Fracao() {}
	public Fracao(Integer numerador, Integer denominador) {
		this.numerador = numerador;
		this.denominador = denominador;
	}
	
	public Integer getNumerador() {
		return numerador;
	}
	public void setNumerador(Integer numerador) {
		this.numerador = numerador;
	}
	public Integer getDenominador() {
		return denominador;
	}
	public void setDenominador(Integer denominador) {
		this.denominador = denominador;
	}
}
