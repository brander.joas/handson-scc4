package br.com.scc4.handson.repository;

import java.util.ArrayList;
import java.util.List;

import br.com.scc4.handson.model.Remetente;

public class RemetenteRepository {
	private static List<Remetente> banco = new ArrayList<>();
	private static Integer lastID = 0;

	static {
		Remetente remetente = new Remetente();
		remetente.setNome("João Silva");
		remetente.setTelefone("(00) 98765-4321");
		remetente.setCpf("012.345.678-90");
		remetente.setEndereco("Rua dos Bobos, número 0");
		remetente.setEmail("joao@example.com");
		remetente.setId(++lastID);
		RemetenteRepository.banco.add(remetente);

		remetente = new Remetente();
		remetente.setNome("Maria Oliveira");
		remetente.setTelefone("(01) 98765-4321");
		remetente.setCpf("012.345.678-91");
		remetente.setEndereco("Rua dos Bobos, número 1");
		remetente.setEmail("maria@example.com");
		remetente.setId(++lastID);
		RemetenteRepository.banco.add(remetente);

		remetente = new Remetente();
		remetente.setNome("José Ferreira");
		remetente.setTelefone("(02) 98765-4321");
		remetente.setCpf("012.345.678-92");
		remetente.setEndereco("Rua dos Bobos, número 2");
		remetente.setEmail("jose@example.com");
		remetente.setId(++lastID);
		RemetenteRepository.banco.add(remetente);

	}

	public Remetente save(Remetente remetente) {
		Remetente searchRemetente = findOne(remetente.getId());
		if(searchRemetente != null) {
			searchRemetente.setNome(remetente.getNome());
			searchRemetente.setTelefone(remetente.getTelefone());
			searchRemetente.setCpf(remetente.getCpf());
			searchRemetente.setEndereco(remetente.getEndereco());
			searchRemetente.setEmail(remetente.getEmail());
		}else {
			searchRemetente = remetente;
			searchRemetente.setId(++lastID);
			banco.add(searchRemetente);
		}
		
		return searchRemetente;
	}

	public Remetente delete(Remetente remetente) {
		Remetente searchRemetente = findOne(remetente.getId());
		if(searchRemetente != null) {
			RemetenteRepository.banco.remove(searchRemetente);
		}
		return searchRemetente;
	}
	
	public List<Remetente> findAll(){
		return RemetenteRepository.banco;
	}
	
	public Remetente findOne(Integer id){
		Remetente remetente = null;
		
		if(id != null) {
			for(Remetente r : RemetenteRepository.banco) {
				if(r.getId() == id) {
					remetente = r;
					break;
				}
			}
		}
		
		return remetente;
	}
}
