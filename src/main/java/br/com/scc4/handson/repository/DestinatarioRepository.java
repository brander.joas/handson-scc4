package br.com.scc4.handson.repository;

import java.util.ArrayList;
import java.util.List;

import br.com.scc4.handson.model.Destinatario;

public class DestinatarioRepository {
	private static List<Destinatario> banco = new ArrayList<>();
	private static Integer lastID = 0;

	static {
		Destinatario destinatario = new Destinatario();
		destinatario.setNome("João Silva");
		destinatario.setTelefone("(00) 98765-4321");
		destinatario.setCpf("012.345.678-90");
		destinatario.setEndereco("Rua dos Bobos, número 0");
		destinatario.setEmail("joao@example.com");
		destinatario.setId(++lastID);
		DestinatarioRepository.banco.add(destinatario);

		destinatario = new Destinatario();
		destinatario.setNome("Maria Oliveira");
		destinatario.setTelefone("(01) 98765-4321");
		destinatario.setCpf("012.345.678-91");
		destinatario.setEndereco("Rua dos Bobos, número 1");
		destinatario.setEmail("maria@example.com");
		destinatario.setId(++lastID);
		DestinatarioRepository.banco.add(destinatario);

		destinatario = new Destinatario();
		destinatario.setNome("José Ferreira");
		destinatario.setTelefone("(02) 98765-4321");
		destinatario.setCpf("012.345.678-92");
		destinatario.setEndereco("Rua dos Bobos, número 2");
		destinatario.setEmail("jose@example.com");
		destinatario.setId(++lastID);
		DestinatarioRepository.banco.add(destinatario);

	}

	public Destinatario save(Destinatario destinatario) {
		Destinatario searchDestinatario = findOne(destinatario.getId());
		if(searchDestinatario != null) {
			searchDestinatario.setNome(destinatario.getNome());
			searchDestinatario.setTelefone(destinatario.getTelefone());
			searchDestinatario.setCpf(destinatario.getCpf());
			searchDestinatario.setEndereco(destinatario.getEndereco());
			searchDestinatario.setEmail(destinatario.getEmail());
		}else {
			searchDestinatario = destinatario;
			searchDestinatario.setId(++lastID);
			banco.add(searchDestinatario);
		}
		
		return searchDestinatario;
	}

	public Destinatario delete(Destinatario destinatario) {
		Destinatario searchDestinatario = findOne(destinatario.getId());
		if(searchDestinatario != null) {
			DestinatarioRepository.banco.remove(searchDestinatario);
		}
		return searchDestinatario;
	}
	
	public List<Destinatario> findAll(){
		return DestinatarioRepository.banco;
	}
	
	public Destinatario findOne(Integer id){
		Destinatario destinatario = null;
		
		for(Destinatario r : DestinatarioRepository.banco) {
			if(r.getId() == id) {
				destinatario = r;
				break;
			}
		}
		
		return destinatario;
	}
}
