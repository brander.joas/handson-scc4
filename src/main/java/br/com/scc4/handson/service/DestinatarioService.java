package br.com.scc4.handson.service;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Service;

import br.com.scc4.handson.model.Destinatario;
import br.com.scc4.handson.repository.DestinatarioRepository;

@Service
public class DestinatarioService {
	private DestinatarioRepository repository;

	public Destinatario incluir(Destinatario destinatario) {
		repository = new DestinatarioRepository();
		Destinatario result = repository.save(destinatario);
		return result;
	}

	public Collection<Destinatario> listar() {
		return new DestinatarioRepository().findAll();
	}

	public Destinatario buscar(Integer id) {
		repository = new DestinatarioRepository();
		Destinatario result = repository.findOne(id);
		return result;
	}

	public Destinatario excluir(Integer id) {
		repository = new DestinatarioRepository();
		Destinatario result = repository.delete(repository.findOne(id));
		return result;
	}

	public Collection<Destinatario> importar(String[][] destinatarios) {
		Collection<Destinatario> result = null;
		if(destinatarios != null) {
			result = new ArrayList<>();
			for(int i = 0; i < destinatarios.length; i++) {
				if(destinatarios[i].length >= 5) {
					Destinatario d = new Destinatario();
					d.setNome(destinatarios[i][0]);
					d.setTelefone(destinatarios[i][1]);
					d.setCpf(destinatarios[i][2]);
					d.setEndereco(destinatarios[i][3]);
					d.setEmail(destinatarios[i][4]);
					incluir(d);
					result.add(d);
				}
			}
		}
		return result;
	}
}
