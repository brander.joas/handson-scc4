package br.com.scc4.handson.service;

import org.springframework.stereotype.Service;

@Service
public class Parte1Service {

	//NEGOCIOS
	public String filterOddNumbersReverse(String param) {
		String[] aux = param.substring(1).substring(0, param.length()-2).split(",");
		StringBuilder sb = new StringBuilder();
		for(int i = aux.length-1; i >= 0; i--) {
			if(Integer.parseInt(aux[i])%2 != 0) {
				sb.append(aux[i]+(i==0?"":","));
			}
		}
		return "{"+sb+"}";
	}

	public String filterEvenNumbersReverse(String param) {
		String[] aux = param.substring(1).substring(0, param.length()-2).split(",");
		StringBuilder sb = new StringBuilder();
		for(int i = aux.length-1; i >= 0; i--) {
			if(Integer.parseInt(aux[i])%2 == 0) {
				sb.append(aux[i]+(i==0?"":","));
			}
		}
		return "{"+sb+"}";
	}

	public String reverseList(String param) {
		String[] aux = param.substring(1).substring(0, param.length()-2).split(",");
		StringBuilder sb = new StringBuilder();
		for(int i = aux.length-1; i >= 0; i--) {
			sb.append(aux[i]+(i==0?"":","));
		}
		return "{"+sb+"}";
	}

	public String countWordLength(String param) {
		return "tamanho = "+param.length();
	}

	public String wordToUpper(String param) {
		return param.toUpperCase();
	}

	public String toBibliographicName(String param) {
		String[] aux = param.split("%");
		StringBuilder sb = new StringBuilder();

		sb.append(aux[aux.length-1].toUpperCase()+", ");
		for(int i = 0; i < aux.length-1; i++) {
			sb.append(aux[i].substring(0, 1).toUpperCase() + aux[i].substring(1)+" ");
		}

		return sb.toString();
	}

	public String filterVowels(String param) {
		StringBuilder sb = new StringBuilder();

		for(int i = 0; i < param.length(); i++) {
			String s = param.substring(i, i+1);
			if(isVowel(s)) {
				sb.append(s);
			}
		}

		return sb.toString();
	}

	public String filterConsonants(String param) {
		StringBuilder sb = new StringBuilder();

		for(int i = 0; i < param.length(); i++) {
			String s = param.substring(i, i+1);
			if(!isVowel(s)) {
				sb.append(s);
			}
		}

		return sb.toString();
	}

	public boolean isVowel(String letter) {
		return (
				letter.toUpperCase().equals("A") || 
				letter.toUpperCase().equals("E") || 
				letter.toUpperCase().equals("I") ||
				letter.toUpperCase().equals("O") || 
				letter.toUpperCase().equals("U")
				);
	}
}