package br.com.scc4.handson.service;

import java.util.Collection;

import org.springframework.stereotype.Service;

import br.com.scc4.handson.model.Destinatario;
import br.com.scc4.handson.model.Mensagem;
import br.com.scc4.handson.model.Remetente;
import br.com.scc4.handson.repository.RemetenteRepository;

@Service
public class RemetenteService {
	private RemetenteRepository repository;
	
	public Remetente incluir(Remetente remetente) {
		repository = new RemetenteRepository();
		Remetente result = repository.save(remetente);
		return result;
	}

	public Collection<Remetente> listar() {
		return new RemetenteRepository().findAll();
	}

	public Remetente buscar(Integer id) {
		repository = new RemetenteRepository();
		Remetente result = repository.findOne(id);
		return result;
	}

	public Remetente excluir(Integer id) {
		repository = new RemetenteRepository();
		Remetente result = repository.delete(repository.findOne(id));
		return result;
	}

	public Mensagem enviarMensagem(Mensagem mensagem) {
		for(Destinatario d : mensagem.getDestinatarios()) {
			System.out.println(
					mensagem.getRemetente().getNome()+
					" enviou :"+"\""+ mensagem.getMensagem() +"\" "
					+ "para: "+d.getNome());
		}
		return mensagem;
	}

}
