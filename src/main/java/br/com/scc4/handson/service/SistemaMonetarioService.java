package br.com.scc4.handson.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class SistemaMonetarioService {
	public String getMinNotasBySaque(int saque) {
		Map<String, Integer> notas = new HashMap<>();
		
		boolean isDivisibleBy3 = false;
		boolean stop = false;
		int count = 0;
		int aux = 0;
		while(!stop) {
			if(count*5 > saque) {
				while(count > 0) {
					aux = saque - (--count)*5; 
					if(aux%3 == 0) {
						isDivisibleBy3 = true;
						break;
					}
				}
				stop = true;
			}else {
				count++;
			}
		}
		
		notas.put("R$5", count);
		notas.put("R$3", isDivisibleBy3 ? (saque - (count*5))/3 : 0);
		
		return (notas.get("R$5")*5 + notas.get("R$3")*3 != saque)? 
				"Não é possível sacar este valor com notas de R$3 e R$5!" 
				: 
				"Saque R$"+saque+": "+notas.get("R$3")+" nota(s) de R$3 e "+notas.get("R$5")+" nota(s) de R$5";
	}
}
