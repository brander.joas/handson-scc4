package br.com.scc4.handson.service;

import org.springframework.stereotype.Service;

import br.com.scc4.handson.model.Fracao;

@Service
public class CalculadoraService {
	//NEGOCIO
	public Fracao soma(Fracao f1, Fracao f2) {
		Fracao result = new Fracao();

		if(f1.getDenominador() == f2.getDenominador()) {
			result.setDenominador(f1.getDenominador());
			result.setNumerador(f1.getNumerador() + f2.getNumerador());
		}else {
			Integer denominador = f1.getDenominador()*f2.getNumerador();
			result.setDenominador(denominador);
			result.setNumerador(
					(denominador / f1.getNumerador() * f1.getDenominador()) 
					+ 
					(denominador / f2.getNumerador() * f2.getDenominador())
					);
		}

		return result;
	}

	public Fracao subtracao(Fracao f1, Fracao f2) {
		Fracao result = new Fracao();

		if(f1.getDenominador() == f2.getDenominador()) {
			result.setDenominador(f1.getDenominador());
			result.setNumerador(f1.getNumerador() - f2.getNumerador());
		}else {
			Integer denominador = f1.getDenominador()*f2.getNumerador();
			result.setDenominador(denominador);
			result.setNumerador(
					(denominador / f1.getNumerador() * f1.getDenominador()) 
					- 
					(denominador / f2.getNumerador() * f2.getDenominador())
					);
		}

		return result;
	}

	public Fracao multiplicacao(Fracao f1, Fracao f2) {
		Fracao result = new Fracao();

		result.setDenominador(f1.getDenominador() * f2.getDenominador());
		result.setNumerador(f1.getNumerador() * f2.getNumerador());

		return result;
	}

	public Fracao divisao(Fracao f1, Fracao f2) {
		return multiplicacao(f1,new Fracao(f2.getDenominador(),f2.getNumerador()));
	}
}