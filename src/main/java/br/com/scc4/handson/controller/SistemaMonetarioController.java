package br.com.scc4.handson.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.handson.service.SistemaMonetarioService;

@RestController
public class SistemaMonetarioController {
	@Autowired
	SistemaMonetarioService service;
	
	//http://localhost:8080/sistemaMonetario?saque=56
	@RequestMapping(method = RequestMethod.GET, value = "/sistemaMonetario", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> getMinNotasBySaqueEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.getMinNotasBySaque(Integer.parseInt(params.get("saque"))), HttpStatus.OK);
	}
}
