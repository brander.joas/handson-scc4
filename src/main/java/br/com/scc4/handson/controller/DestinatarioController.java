package br.com.scc4.handson.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.handson.model.Destinatario;
import br.com.scc4.handson.service.DestinatarioService;

@CrossOrigin(origins  = "http://localhost:4200")
@RestController
public class DestinatarioController {
	
	@Autowired
	DestinatarioService service;
	
	@RequestMapping(method = RequestMethod.POST, value = "/destinatario", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Destinatario> incluir(@RequestBody Destinatario destinatario) {
		Destinatario destinatarioCadastrado = service.incluir(destinatario);
		return new ResponseEntity<Destinatario>(destinatarioCadastrado, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/destinatario", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Destinatario> editar(@RequestBody Destinatario destinatario){
		Destinatario destinatarioCadastrado = service.incluir(destinatario);		
		return new ResponseEntity<Destinatario>(destinatarioCadastrado, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/destinatario", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Destinatario>> listar(@RequestBody(required=false) Destinatario destinatario) {
		return new ResponseEntity<>(service.listar(), HttpStatus.OK);
	}
 
	@RequestMapping(value="/destinatario/{id}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Destinatario> buscar(@PathVariable("id") Integer id){
		return new ResponseEntity<Destinatario>(service.buscar(id), HttpStatus.OK);
	}
 
	@RequestMapping(value="/destinatario/{id}", method = RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Destinatario> excluir(@PathVariable("id") Integer id){
		return new ResponseEntity<Destinatario>(service.excluir(id), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/import", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Destinatario>> incluir(@RequestBody String[][] destinatarios) {
		return new ResponseEntity<>(service.importar(destinatarios), HttpStatus.CREATED);
	}
}
