package br.com.scc4.handson.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.handson.service.Parte1Service;

@RestController
public class Parte1Controller {
	@Autowired
	Parte1Service service;
	
	//END POINTS
	@RequestMapping(method = RequestMethod.GET, value = "/listaReversa", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> reverseListEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.reverseList(params.get("lista")), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/imprimirImpares", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> filterOddNumbersEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.filterOddNumbersReverse(params.get("lista")), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/imprimirPares", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> filterEvenNumbersEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.filterEvenNumbersReverse(params.get("lista")), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/tamanho", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> countWordLengthEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.countWordLength(params.get("palavra")), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/maiusculas", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> wordToUpperEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.wordToUpper(params.get("palavra")), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/vogais", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> filterVowelsEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.filterVowels(params.get("palavra")), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/consoantes", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> filterConsonantsEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.filterConsonants(params.get("palavra")), HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/nomeBibliografico", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> toBibliographicNameEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		return new ResponseEntity<>(service.toBibliographicName(params.get("nome")), HttpStatus.OK);
	}
}