package br.com.scc4.handson.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.handson.model.Mensagem;
import br.com.scc4.handson.model.Remetente;
import br.com.scc4.handson.service.RemetenteService;

@CrossOrigin(origins  = "http://localhost:4200")
@RestController
public class RemetenteController {
	
	@Autowired
	RemetenteService service;
	
	@RequestMapping(method = RequestMethod.POST, value = "/remetente", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Remetente> incluir(@RequestBody Remetente remetente) {
		Remetente remetenteCadastrado = service.incluir(remetente);
		return new ResponseEntity<Remetente>(remetenteCadastrado, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/remetente", method = RequestMethod.PUT, consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Remetente> editar(@RequestBody Remetente remetente){
		Remetente remetenteCadastrado = service.incluir(remetente);		
		return new ResponseEntity<Remetente>(remetenteCadastrado, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/remetente", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Collection<Remetente>> listar(@RequestBody(required=false) Remetente remetente) {
		return new ResponseEntity<>(service.listar(), HttpStatus.OK);
	}
 
	@RequestMapping(value="/remetente/{id}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Remetente> buscar(@PathVariable("id") Integer id){
		return new ResponseEntity<Remetente>(service.buscar(id), HttpStatus.OK);
	}
 
	@RequestMapping(value="/remetente/{id}", method = RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Remetente> excluir(@PathVariable("id") Integer id){
		return new ResponseEntity<Remetente>(service.excluir(id), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/remetente/send", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Mensagem> enviarMensagem(@RequestBody Mensagem mensagem) {
		System.out.println(mensagem);
		return new ResponseEntity<Mensagem>(service.enviarMensagem(mensagem), HttpStatus.CREATED);
	}
}
