package br.com.scc4.handson.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.scc4.handson.model.Fracao;
import br.com.scc4.handson.service.CalculadoraService;

@RestController
public class CalculadoraController {
	
	@Autowired
	CalculadoraService service;
	
	//End Points
	@RequestMapping(method = RequestMethod.GET, value = "/somaFracao", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> somaEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		Fracao f1 = new Fracao();
		String[] paramsF1 = params.get("f1").split("/");
		f1.setNumerador(Integer.parseInt(paramsF1[0]));
		f1.setDenominador(Integer.parseInt(paramsF1[1]));
		
		Fracao f2 = new Fracao();
		String[] paramsF2 = params.get("f2").split("/");
		f2.setNumerador(Integer.parseInt(paramsF2[0]));
		f2.setDenominador(Integer.parseInt(paramsF2[1]));
		
		Fracao result = service.soma(f1,f2);
		
		return new ResponseEntity<>("Resultado= "+result.getNumerador()+"/"+result.getDenominador(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/subtraiFracao", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> subtracaoEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		Fracao f1 = new Fracao();
		String[] paramsF1 = params.get("f1").split("/");
		f1.setNumerador(Integer.parseInt(paramsF1[0]));
		f1.setDenominador(Integer.parseInt(paramsF1[1]));
		
		Fracao f2 = new Fracao();
		String[] paramsF2 = params.get("f2").split("/");
		f2.setNumerador(Integer.parseInt(paramsF2[0]));
		f2.setDenominador(Integer.parseInt(paramsF2[1]));
		
		Fracao result = service.subtracao(f1,f2);
		
		return new ResponseEntity<>("Resultado= "+result.getNumerador()+"/"+result.getDenominador(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/multiplicaFracao", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> multiplicacaoEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		Fracao f1 = new Fracao();
		String[] paramsF1 = params.get("f1").split("/");
		f1.setNumerador(Integer.parseInt(paramsF1[0]));
		f1.setDenominador(Integer.parseInt(paramsF1[1]));
		
		Fracao f2 = new Fracao();
		String[] paramsF2 = params.get("f2").split("/");
		f2.setNumerador(Integer.parseInt(paramsF2[0]));
		f2.setDenominador(Integer.parseInt(paramsF2[1]));
		
		Fracao result = service.multiplicacao(f1,f2);
		
		return new ResponseEntity<>("Resultado= "+result.getNumerador()+"/"+result.getDenominador(), HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/divideFracao", 
			produces=MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> divisaoEndPoint(@RequestBody(required=false) Object obj, @RequestParam  Map<String, String> params) {
		Fracao f1 = new Fracao();
		String[] paramsF1 = params.get("f1").split("/");
		f1.setNumerador(Integer.parseInt(paramsF1[0]));
		f1.setDenominador(Integer.parseInt(paramsF1[1]));
		
		Fracao f2 = new Fracao();
		String[] paramsF2 = params.get("f2").split("/");
		f2.setNumerador(Integer.parseInt(paramsF2[0]));
		f2.setDenominador(Integer.parseInt(paramsF2[1]));
		
		Fracao result = service.divisao(f1,f2);
		
		return new ResponseEntity<>("Resultado= "+result.getNumerador()+"/"+result.getDenominador(), HttpStatus.OK);
	}
}
