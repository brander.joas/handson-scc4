import { Component, OnInit } from '@angular/core';
 
import {Router} from '@angular/router';
 
import {DestinatarioService} from '../../services/destinatario.service';
 
import {Destinatario} from '../../services/destinatario';
 
@Component({
    selector: 'app-consulta-destinatario',
    templateUrl: './d_consulta.component.html',
    styleUrls:["./d_consulta.component.css"]
  })
  export class ConsultaDestinatarioComponent implements OnInit {
 
    private destinatarios: Destinatario[] = new Array();
    private titulo:string;
 
    constructor(private destinatarioService: DestinatarioService,
                private router: Router){}
 
    ngOnInit() {
      this.titulo = "Destinatarios Cadastrados";
      this.destinatarioService.getDestinatarios().subscribe(res => this.destinatarios = res);
    }
 
    excluir(id:number, index:number):void {
      if(confirm("Deseja realmente excluir esse registro?")){
        this.destinatarioService.excluirDestinatario(id).subscribe(response => {
            this.destinatarios.splice(index,1);
          },
          (erro) => {                    
            alert(erro);
          });        
      }
    }
 
    editar(id:number):void{
      this.router.navigate(['/cadastro-destinatario',id]);
 
    }
 
  }