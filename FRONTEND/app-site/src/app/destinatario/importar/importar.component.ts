import { Component, OnInit } from '@angular/core';
 
import { Papa } from 'ngx-papaparse';

import {Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
 
import {DestinatarioService} from '../../services/destinatario.service';
 
import {Destinatario} from '../../services/destinatario';
 
import { Observable } from 'rxjs';
 
@Component({
    selector: 'app-importar-destinatario',
    templateUrl: './importar.component.html',
    styleUrls:["./importar.component.css"]
  })
  export class ImportarDestinatarioComponent implements OnInit {
    private titulo:string;
    private destinatario:Destinatario = new Destinatario();
    private csvTableHeader;
    private csvTableData;
 
    constructor(private papa: Papa,
                private destinatarioService: DestinatarioService,
                private router: Router,
                private activatedRoute: ActivatedRoute){}
 
    ngOnInit() {
      this.titulo = "Importar CSV de Destinatarios";
    }
 
    salvar():void {
      console.log('SALVAR');
      console.log(this.csvTableData);
      this.destinatarioService.processaCSV(this.csvTableData).subscribe(response => {
        this.router.navigate(['/consulta-destinatario']);
     },
     (erro) => {                    
        alert(erro);
     });
    }

    fileChangeListener($event: any): void {
      const files = $event.srcElement.files;

      if (files !== null && files !== undefined && files.length > 0) {
        const reader: FileReader = new FileReader();
        reader.readAsText(files[0]);
        reader.onload = e => {

          const csv = reader.result;
          const results = this.papa.parse(csv as string, { header: false });

          if (results !== null && results !== undefined && results.data !== null &&
            results.data !== undefined && results.data.length > 0 && results.errors.length === 0) {

            this.csvTableHeader = results.data[0];
            this.csvTableData = [...results.data.slice(1, results.data.length)];
          } else {
            for (let i = 0; i < results.errors.length; i++) {
              console.log( 'Error Parsing CSV File: ',results.errors[i].message);
            }
          }
        };
      } else {
        console.log('No File Selected');
      }
    }
  }