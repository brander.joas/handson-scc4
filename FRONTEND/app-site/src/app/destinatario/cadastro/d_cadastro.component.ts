import { Component, OnInit } from '@angular/core';
 
import {Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
 
import {DestinatarioService} from '../../services/destinatario.service';
 
import {Destinatario} from '../../services/destinatario';
 
import { Observable } from 'rxjs';
 
@Component({
    selector: 'app-cadastro-destinatario',
    templateUrl: './d_cadastro.component.html',
    styleUrls:["./d_cadastro.component.css"]
  })
  export class CadastroDestinatarioComponent implements OnInit {
    public maskTelefone = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    public maskCPF = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/,'-', /\d/, /\d/];
    private titulo:string;
    private destinatario:Destinatario = new Destinatario();
 
    constructor(private destinatarioService: DestinatarioService,
                private router: Router,
                private activatedRoute: ActivatedRoute){}
 
    ngOnInit() {
      this.activatedRoute.params.subscribe(parametro=>{
        if(parametro["id"] == undefined){
          this.titulo = "Novo Cadastro de Destinatario";
        }
        else{
 
          this.titulo = "Editar Cadastro de Destinatario";
          this.destinatarioService.getDestinatario(Number(parametro["id"])).subscribe(res => this.destinatario = res);
        }
      });      
    }
 
    salvar():void {
 
      if(this.destinatario.id == undefined){
        this.destinatarioService.addDestinatario(this.destinatario).subscribe(response => {
            this.destinatario = new Destinatario();
         },
         (erro) => {   
            alert(erro);
         });
 
      }
      else{
        this.destinatarioService.atualizarDestinatario(this.destinatario).subscribe(response => {
          this.router.navigate(['/consulta-destinatario']);
       },
       (erro) => {                    
          alert(erro);
       });
      }
 
    }
 
  }