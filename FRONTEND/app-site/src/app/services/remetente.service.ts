import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers} from '@angular/http';
import { RequestOptions } from '@angular/http';
/* 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';*/
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';
 
import {Remetente} from './remetente';
import {ConfigService} from './config.service';
import { Mensagem } from './mensagem';
 
@Injectable()
export class RemetenteService {
 
    private baseUrlService:string = '';
    private headers:Headers;
    private options:RequestOptions;
 
    constructor(private http: Http,
                private configService: ConfigService) { 
 
        this.baseUrlService = configService.getUrlService() + '/remetente/';
 
        this.headers = new Headers({ 'Content-Type': 'application/json;charset=UTF-8' });                
        this.options = new RequestOptions({ headers: this.headers });
    }
 
    getRemetentes(){        
        return this.http.get(this.baseUrlService).pipe(map(res => res.json()));
    }
 
    addRemetente(remetente: Remetente){
        return this.http.post(this.baseUrlService, JSON.stringify(remetente),this.options)
        .pipe(map(res => res.json()));
    }
 
    excluirRemetente(id:number){
        return this.http.delete(this.baseUrlService + id).pipe(map(res => res.json()));
    }
 
    getRemetente(id:number){
        return this.http.get(this.baseUrlService + id).pipe(map(res => res.json()));
    }
 
    atualizarRemetente(remetente:Remetente){
        return this.http.put(this.baseUrlService, JSON.stringify(remetente),this.options)
        .pipe(map(res => res.json()));
    }
    
    enviarMensagens(mensagem: Mensagem){
        return this.http.post(this.baseUrlService+'/send/', JSON.stringify(mensagem),this.options)
        .pipe(map(res => res.json()));
    }
}