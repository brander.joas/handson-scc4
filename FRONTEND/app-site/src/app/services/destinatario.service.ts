import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers} from '@angular/http';
import { RequestOptions } from '@angular/http';
/* 
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';*/
import { map } from "rxjs/operators";
import { Observable } from 'rxjs';
 
import {Destinatario} from './destinatario';
import {ConfigService} from './config.service';
 
@Injectable()
export class DestinatarioService {
 
    private baseUrlService:string = '';
    private headers:Headers;
    private options:RequestOptions;
 
    constructor(private http: Http,
                private configService: ConfigService) { 
 
        this.baseUrlService = configService.getUrlService() + '/destinatario/';
 
        this.headers = new Headers({ 'Content-Type': 'application/json;charset=UTF-8' });                
        this.options = new RequestOptions({ headers: this.headers });
    }
    
    processaCSV(csvTableData){
        return this.http.post(this.configService.getUrlService()+'/import', JSON.stringify(csvTableData),this.options)
        .pipe(map(res => res.json()));
    }

    getDestinatarios(){        
        return this.http.get(this.baseUrlService).pipe(map(res => res.json()));
    }
 
    addDestinatario(destinatario: Destinatario){
        return this.http.post(this.baseUrlService, JSON.stringify(destinatario),this.options)
        .pipe(map(res => res.json()));
    }
 
    excluirDestinatario(id:number){
        return this.http.delete(this.baseUrlService + id).pipe(map(res => res.json()));
    }
 
    getDestinatario(id:number){
        return this.http.get(this.baseUrlService + id).pipe(map(res => res.json()));
    }
 
    atualizarDestinatario(destinatario:Destinatario){
        return this.http.put(this.baseUrlService, JSON.stringify(destinatario),this.options)
        .pipe(map(res => res.json()));
    }
}