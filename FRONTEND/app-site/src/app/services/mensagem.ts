import {Remetente} from './remetente';
import {Destinatario} from './destinatario';

export class Mensagem{
    remetente: Remetente;
    destinatarios: Destinatario[];
    mensagem:string;
}