import { Component, OnInit } from '@angular/core';
 
import {Router} from '@angular/router';
 
import {DestinatarioService} from '../services/destinatario.service';
 
import {Destinatario} from '../services/destinatario';

import {RemetenteService} from '../services/remetente.service';
 
import {Remetente} from '../services/remetente';
 
import {Mensagem} from '../services/mensagem';
 
@Component({
    selector: 'app-msg',
    templateUrl: './msg.component.html',
    styleUrls:["./msg.component.css"]
  })
  export class MensagemComponent implements OnInit {
 
    private destinatarios: Destinatario[] = new Array();
    private remetentes: Remetente[] = new Array()
    private destinatariosSelecionados: Destinatario[] = new Array();
    private remetente: Remetente;
    private titulo:string;
    private msg:string;
 
    constructor(private destinatarioService: DestinatarioService,
                private remetenteService: RemetenteService,
                private router: Router){}
 
    ngOnInit() {
 
      /*SETA O TÍTULO */
      this.titulo = "Enviar Mensagens";
 
      /*CHAMA O SERVIÇO E RETORNA TODAS AS PESSOAS CADASTRADAS */
      this.destinatarioService.getDestinatarios().subscribe(res => this.destinatarios = res);
      this.remetenteService.getRemetentes().subscribe(res => this.remetentes = res);
    }

    marcarDestinatario(destinatario: Destinatario){
      let find:boolean = false;
      this.destinatariosSelecionados.forEach(element => {
        if(element.id === destinatario.id){
          find = true;
        }
      });

      if(!find){
        this.destinatariosSelecionados.push(destinatario)
      }else{
        this.destinatariosSelecionados.splice( this.destinatariosSelecionados.indexOf(destinatario), 1 );
      }
      console.log(find);
      console.log(this.destinatariosSelecionados)
    }

    marcarRemetente(remetente: Remetente){
      this.remetente = remetente;
      console.log(Remetente)
    }

    enviar(){
      let mensagem: Mensagem = new Mensagem();
      mensagem.destinatarios = this.destinatariosSelecionados;
      mensagem.remetente = this.remetente;
      mensagem.mensagem = this.msg;
      this.remetenteService.enviarMensagens(mensagem).subscribe(res => {
        this.remetente = new Remetente;
        this.destinatariosSelecionados = new Array();
        this.msg = '';
        this.router.navigate(['/enviar-mensagem']);
      });
    }
  }