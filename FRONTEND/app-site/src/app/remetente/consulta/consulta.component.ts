import { Component, OnInit } from '@angular/core';
 
import {Router} from '@angular/router';
 
import {RemetenteService} from '../../services/remetente.service';
 
import {Remetente} from '../../services/remetente';

@Component({
    selector: 'app-consulta-remetente',
    templateUrl: './consulta.component.html',
    styleUrls:["./consulta.component.css"]
  })
  export class ConsultaRemetenteComponent implements OnInit {
 
    private remetentes: Remetente[] = new Array();
    private titulo:string;
 
    constructor(private remetenteService: RemetenteService,
                private router: Router){}
 
    ngOnInit() {
      this.titulo = "Remetentes Cadastrados";
      this.remetenteService.getRemetentes().subscribe(res => this.remetentes = res);
    }
 
    excluir(codigo:number, index:number):void {
      if(confirm("Deseja realmente excluir esse registro?")){
        this.remetenteService.excluirRemetente(codigo).subscribe(response => {
            this.remetentes.splice(index,1);
          },
          (erro) => {                    
               alert(erro);
          });        
      }
 
    }
 
    editar(id:number):void{
      this.router.navigate(['/cadastro-remetente',id]);
 
    }
  }