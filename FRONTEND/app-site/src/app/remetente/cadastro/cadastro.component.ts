import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
 
import {RemetenteService} from '../../services/remetente.service';
 
import {Remetente} from '../../services/remetente';
 
import { Observable } from 'rxjs';
 
@Component({
    selector: 'app-cadastro-remetente',
    templateUrl: './cadastro.component.html',
    styleUrls:["./cadastro.component.css"]
  })
  export class CadastroRemetenteComponent implements OnInit {
    public maskTelefone = ['(', /[1-9]/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
    public maskCPF = [/\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/,'-', /\d/, /\d/];
    private titulo:string;
    private remetente:Remetente = new Remetente();
    
    constructor(private remetenteService: RemetenteService,
                private router: Router,
                private activatedRoute: ActivatedRoute){}
 
    ngOnInit() {
      this.activatedRoute.params.subscribe(parametro=>{
 
        if(parametro["id"] == undefined){
          this.titulo = "Novo Cadastro de Remetente";
        }
        else{
          this.titulo = "Editar Cadastro de Remetente";
          this.remetenteService.getRemetente(Number(parametro["id"])).subscribe(res => this.remetente = res);
        }
      });      
    }
 
    salvar():void {
      if(this.remetente.id == undefined){
        this.remetenteService.addRemetente(this.remetente).subscribe(response => {
            this.remetente = new Remetente();
         },
         (erro) => {   
            alert(erro);
         });
      }
      else{
        this.remetenteService.atualizarRemetente(this.remetente).subscribe(response => {
          this.router.navigate(['/consulta-remetente']);
       },
       (erro) => {                    
          alert(erro);
       });
      }
    }
  }