import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { PapaParseModule } from 'ngx-papaparse';

import { AppComponent } from './app.component';
import {MenuComponent} from './menu/menu.component';
import {HomeComponent} from './home/home.component';
import {CadastroRemetenteComponent} from './remetente/cadastro/cadastro.component';
import {ConsultaRemetenteComponent} from './remetente/consulta/consulta.component';
import {CadastroDestinatarioComponent} from './destinatario/cadastro/d_cadastro.component';
import {ConsultaDestinatarioComponent} from './destinatario/consulta/d_consulta.component';
import {ImportarDestinatarioComponent} from './destinatario/importar/importar.component';
import {MensagemComponent} from './mensagem/msg.component';

import {ConfigService} from './services/config.service';
import {RemetenteService} from './services/remetente.service';
import { DestinatarioService } from './services/destinatario.service';

import { routing } from './../app.routes';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    ConsultaRemetenteComponent,
    CadastroRemetenteComponent,
    CadastroDestinatarioComponent,
    ConsultaDestinatarioComponent,
    ImportarDestinatarioComponent,
    MensagemComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    TextMaskModule,
    PapaParseModule,
    routing
  ],
  providers: [ConfigService,RemetenteService, DestinatarioService],
  bootstrap: [AppComponent]
})
export class AppModule { }
