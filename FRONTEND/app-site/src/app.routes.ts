import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
 
import { ConsultaRemetenteComponent } from './app/remetente/consulta/consulta.component';
 
import {CadastroRemetenteComponent} from './app/remetente/cadastro/cadastro.component';
 
import { ConsultaDestinatarioComponent } from './app/destinatario/consulta/d_consulta.component';
 
import {CadastroDestinatarioComponent} from './app/destinatario/cadastro/d_cadastro.component';
import {ImportarDestinatarioComponent} from './app/destinatario/importar/importar.component';
import {MensagemComponent} from './app/mensagem/msg.component';

import { HomeComponent } from './app/home/home.component';
 
const appRoutes: Routes = [
    { path: 'home',                      component: HomeComponent },
    { path: '',                          component: HomeComponent },
    { path: 'consulta-remetente',        component: ConsultaRemetenteComponent },
    { path: 'cadastro-remetente',        component: CadastroRemetenteComponent },
    { path: 'cadastro-remetente/:id',    component: CadastroRemetenteComponent },
    { path: 'consulta-destinatario',     component: ConsultaDestinatarioComponent },
    { path: 'cadastro-destinatario',     component: CadastroDestinatarioComponent },
    { path: 'cadastro-destinatario/:id', component: CadastroDestinatarioComponent },
    { path: 'importar-destinatario',     component: ImportarDestinatarioComponent },
    { path: 'enviar-mensagem',           component: MensagemComponent }
 
];
 
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);